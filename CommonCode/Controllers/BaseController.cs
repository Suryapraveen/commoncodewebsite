﻿#region NameSpaces
using CommonCode.App_Start;
using System.Web.Mvc;
#endregion
#region CommonCode.Controllers
namespace CommonCode.Controllers
{
    #region BaseController
    // GET: Base
    [CommonCodeAuthorizeAttribute]
    public class BaseController : Controller
    {

    } 
    #endregion

} 
#endregion