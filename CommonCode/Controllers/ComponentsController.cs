﻿#region NameSpaces
using CommonCode.BusinessLayer.BAL;
using System.Web.Mvc;
#endregion
#region CommonCode.Controllers
namespace CommonCode.Controllers
{
    #region ComponentsController
    public class ComponentsController : BaseController
    {
        #region List
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region GetComponents
        public JsonResult GetComponents()
        {
            return Json((new ComponentsBAL().GetComponents()), JsonRequestBehavior.AllowGet);
        }
        #endregion
        
    }
    #endregion
}
#endregion