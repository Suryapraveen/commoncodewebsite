﻿#region NameSpaces
using CommonCode.App_Start;
using CommonCode.BusinessLayer.BAL;
using CommonCode.Common.Helpers;
using CommonCode.Entities;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web.Mvc;
#endregion

#region CommonCode.Controllers
namespace CommonCode.Controllers
{
    #region ProjectsController
    public class ProjectsController : BaseController
    {
        #region Index
        // GET: Projects
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        #endregion

        #region List
        [HttpGet]
        public ActionResult List()
        {
            return View();
        }
        #endregion

        #region GetAllProjects
        [HttpPost]
        public JsonResult GetAllProjects(GridSearchParameters projectSearch)
        {
            List<object> lst = new ProjectsBAL().GetProjectList(projectSearch);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SaveOrUpdateProejctInformation
        [HttpPost]
        public JsonResult SaveOrUpdateProejctInformation(ProjectModel pm)
        {
            TempData["ProjectItems"] = pm.ProjectCompItems;
            TempData["ProjectName"] = pm.ProjectName;
            TempData.Keep();
            return Json(new ProjectsBAL().SaveOrUpdateProejctInformation(pm), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DownloadZipFile
        [HttpGet]
        public ActionResult DownloadZipFile()
        {
            if (TempData["ProjectItems"] != null)
            {
                string PrjItems = (string)TempData["ProjectItems"];
                Dictionary<string, string[]> dic = new Dictionary<string, string[]>();
                List<string> lstValues = new List<string>();
                if (PrjItems.Split(',').Contains("1") || PrjItems.Split(',').Contains("2"))
                {
                    lstValues = new List<string>();
                    if (PrjItems.Split(',').Contains("1"))
                        lstValues.Add(Server.MapPath("~/Files/DB/ResetDB.sql"));
                    if (PrjItems.Split(',').Contains("2"))
                        lstValues.Add(Server.MapPath("~/Files/DB/Split.sql"));
                    dic.Add("Database", lstValues.ToArray());
                }
                if (PrjItems.Split(',').Contains("3") || PrjItems.Split(',').Contains("4"))
                {
                    lstValues = new List<string>();
                    if (PrjItems.Split(',').Contains("3"))
                        lstValues.Add(Server.MapPath("~/Files/DLLS/ExceptionHandling.dll"));
                    if (PrjItems.Split(',').Contains("4"))
                        lstValues.Add(Server.MapPath("~/Files/DLLS/EmailService.dll"));
                    dic.Add("DLLS", lstValues.ToArray());
                }
                if (PrjItems.Split(',').Contains("5"))
                    dic.Add("MVC", new string[] { Server.MapPath("~/Files/MVC") });
                if (PrjItems.Split(',').Contains("6"))
                    dic.Add("Scripts", new string[] { Server.MapPath("~/Files/Scripts") });
                  if (PrjItems.Split(',').Contains("7"))
                    dic.Add("CSS", new string[] { Server.MapPath("~/Files/CSS") });
                return new ZipResult(dic, ((string)TempData["ProjectName"] + "_" + DateTime.Now.ToString("ddMMyyyyHHmmsstt")) + ".zip");
            }
            return null;
        }
        #endregion
    }
    #endregion
}
#endregion