﻿CommonCodeApp.controller('ComponentsController', function ($http, $scope, $location, modalService) {
    $scope.selectedIndex = -1;
    var req = {
        method: 'POST',
        url: getBaseUrl() + '/Components/GetComponents',
        contentType: "application/json"
      }
    $http(req).success(function (data) {
        $scope.Components = data[0];
        $scope.ComponentItems = data[1];
    }).error(function (data, status, headers, config) {
        modalService.show('Error Occurred', StatusType.ERROR);
    });

});