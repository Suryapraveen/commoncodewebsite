﻿CommonCodeApp.controller('LoginController', function ($http, $scope, $location, modalService) {
    $scope.LoginId = '';
    $scope.UserPassword = '';

    $scope.submitForm = function () {
      
        if ($scope.loginForm.$valid) {
            var LoginModel = {
                UserName: $scope.LoginId,
                Password: $scope.UserPassword
            };
            var req = {
                method: 'POST',
                url: getBaseUrl() + '/Login/ValidateUserLogin',
                contentType: "application/json",
                data: LoginModel
            }
            $http(req).success(function (data) {
                if (data.Result == '1') {
                    window.location.href = getBaseUrl() + data.URL;
                }
                else if (data.Result == '2') {
                    modalService.show('User Name wrong', StatusType.ERROR);
                }
                else if (data.Result == '3') {
                    modalService.show('Password cant be empty', StatusType.ERROR);
                }
                else if (data.Result == '4') {
                    modalService.show('Password wrong', StatusType.ERROR);
                }
                else if (data.Result == '5') {
                    modalService.show('User is inactive', StatusType.ERROR);
                }
                else {
                    modalService.show('Error Occurred', StatusType.ERROR);
                }
            }).error(function (data, status, headers, config) {
                modalService.show('Error Occurred', StatusType.ERROR);
            });
        }

    }

  
});