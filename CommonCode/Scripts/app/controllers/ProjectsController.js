﻿CommonCodeApp.controller('ProjectsController', function ($http, $scope, $location, modalService, $mdDialog, $filter) {

    $scope.filterBy = '';
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;
    $scope.AddButtonText = 'Save & Download';
    $scope.HeaderText = 'Project Information';
    $scope.SelectedItems = [];
    $scope.ComponentItems = [];
    $scope.selectedIndex = 0;

    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadProjectsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadProjectsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadProjectsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadProjectsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        var projectSearch = {
            currentPage: currentPage,
            pageItems: pageItems,
            filterBy: filterBy,
            filterByFields: angular.toJson(filterByFields),
            orderBy: orderBy,
            orderByReverse: orderByReverse
        }

        pCurrentPage = currentPage;
        pPageItems = pageItems;
        pOrderBy = orderBy;
        pOrderByReverse = orderByReverse

        var req = {
            method: 'POST',
            url: getBaseUrl() + '/Projects/GetAllProjects?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: JSON.stringify(projectSearch)
        }
        $http(req).success(function (data) {
            $scope.Projects = data[0].ListOfItems;
            $scope.TotalRecordsCount = data[0].TotalRecordCount;
            $scope.Components = data[1];
            $scope.ComponentItems = data[2];
        }).error(function (error) {
            modalService.show(errorMessage, StatusType.ERROR);
        });
    }

    $scope.toggle = function (item) {

        var idx = $scope.SelectedItems.indexOf(parseInt(item));
        if (idx > -1) {
            if ($filter('filter')($scope.ComponentItems, { ComponentItemID: parseInt(item) }, true)[0] != undefined)
                $filter('filter')($scope.ComponentItems, { ComponentItemID: parseInt(item) }, true)[0].Selected = 0;
            $scope.SelectedItems.splice(idx, 1);
        }
        else {
            if ($filter('filter')($scope.ComponentItems, { ComponentItemID: parseInt(item) }, true)[0] != undefined)
                $filter('filter')($scope.ComponentItems, { ComponentItemID: parseInt(item) }, true)[0].Selected = 1;
            $scope.SelectedItems.push(parseInt(item));
        }
    };


    $scope.SaveOrUpdateInfo = function () {

        if ($scope.ProjectDetails.$valid && $scope.SelectedItems.length > 0) {

            var projectCompItems = '';
            angular.forEach($scope.SelectedItems, function (value) {
                projectCompItems += value + ',';
            });

            var pm = {

                ProjectId: $scope.ProjectID,
                ProjectName: $scope.ProjectName,
                ProjectDescription: $scope.ProjectDescription,
                ProjectCompItems: projectCompItems
            }
            var req = {
                method: 'POST',
                url: getBaseUrl() + '/Projects/SaveOrUpdateProejctInformation?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(pm)
            }
            $http(req).success(function (data) {
                if ((data != undefined && data == true)) {
                    modalService.show(($scope.ProjectID == 0 ? " Added " : "Updated ") + "Successfully", StatusType.SUCCESS);
                    window.location = getBaseUrl() + '/Projects/DownloadZipFile?nocache=' + new Date().getTime();
                    LoadProjectsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                    $mdDialog.hide();
                }
                else {
                    modalService.show(errorMessage, StatusType.ERROR);
                }
            }).error(function (error) {
                modalService.show(errorMessage, StatusType.ERROR);
            });
        }

    }

    $scope.clearFields = function () {
        $scope.ProjectID = 0;
        $scope.ProjectName = $scope.ProjectDescription = '';
        $scope.SelectedItems = [];
        $scope.AddButtonText = 'Save & Download';
        $scope.HeaderText = 'Project Information';
        if ($scope.ComponentItems.length > 1)
            $scope.ComponentItems.forEach(function (obj) {
                obj.Selected = 0;
            });
    }

    $scope.closeDialog = function () {
        $mdDialog.hide();
    }

    $scope.showTabDialog = function (ev, grd) {
        $scope.selectedIndex = 0;
        $scope.clearFields();
        if (grd != null) {
            if (grd.PrjectCompItems != null && grd.PrjectCompItems != undefined) {
                angular.forEach(grd.PrjectCompItems.split(','), function (value) {
                    var idx = $scope.SelectedItems.indexOf(parseInt(value.trim()));
                    if (idx > -1) {
                        $scope.SelectedItems.splice(idx, 1);
                    }
                    else {
                        $scope.SelectedItems.push(parseInt(value.trim()));
                    }
                    if ($filter('filter')($scope.ComponentItems, { ComponentItemID: parseInt(value.trim()) }, true)[0] != undefined)
                        $filter('filter')($scope.ComponentItems, { ComponentItemID: parseInt(value.trim()) }, true)[0].Selected = 1;
                });
            }
            $scope.ProjectID = grd.ProjectID;
            $scope.ProjectName = grd.ProjectName;
            $scope.ProjectDescription = grd.Description;
            $scope.AddButtonText = 'Update & Download';
            $scope.HeaderText = 'Edit Project Information';
        }
        $mdDialog.show({
            contentElement: '#ComponentsList',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            escapeToClose: true
        });
    };
});