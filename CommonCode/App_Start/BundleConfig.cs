﻿#region NameSpaces
using System.Web;
using System.Web.Optimization;
#endregion
#region CommonCode
namespace CommonCode
{
    #region BundleConfig
    public class BundleConfig
    {
        #region RegisterBundles
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                                           "~/Scripts/jquery-{version}.js",
                                           "~/Scripts/angular/angular.min.js",
                                            "~/Scripts/angular-route.min.js",
                                            "~/Scripts/angular-animate/angular-animate.min.js",
                                            "~/Scripts/angular-aria/angular-aria.min.js",
                                            "~/Scripts/angular-material/angular-material.min.js",
                                            "~/Scripts/angular-messages.min.js",
                                            "~/Scripts/trNgGrid/trNgGrid.js",
                                            "~/Scripts/angular-spinner/spin.min.js",
                                            "~/Scripts/angular-spinner/angular-loading-spinner.js",
                                            "~/Scripts/angular-spinner/angular-spinner.min.js",
                                            "~/Scripts/app/app.js",
                                            "~/Scripts/app/services/modalService.js",
                                            "~/Scripts/app/controllers/ProjectsController.js"
                                       ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/trNgGrid.min.css",
                      "~/Content/angular-material.min.css",
                      "~/Content/site.css"));
        } 
        #endregion
    } 
    #endregion
}

#endregion