﻿#region NameSpaces
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web.Mvc;
#endregion
#region CommonCode.App_Start
namespace CommonCode.App_Start
{
    #region ZipResult
    public class ZipResult : ActionResult
    {
        #region Global Variables
        public Dictionary<string, string[]> dicPath { get; private set; }
        public string Filename { get; private set; }
        #endregion

        #region ZipResult
        public ZipResult(Dictionary<string, string[]> dic, string filename)
        {
            dicPath = dic;
            Filename = filename;
        }
        #endregion

        #region ExecuteResult
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            var response = context.HttpContext.Response;
            response.ContentType = "application/zip";
            response.BufferOutput = false;
            using (var zip = new ZipFile())
            {
                foreach (KeyValuePair<string, string[]> kvp in dicPath)
                {
                    if (new string[] { "mvc", "scripts", "css" }.Contains(kvp.Key.ToLower()))
                    {
                        zip.AddDirectory(kvp.Value[0]);
                    }
                    else
                    {
                        zip.AddDirectoryByName(kvp.Key);
                        zip.AddFiles(kvp.Value, kvp.Key);
                    }
                }
                var cd = new ContentDisposition
                {
                    FileName = Filename,
                    Inline = false
                };
                response.Headers.Add("Content-Disposition", cd.ToString());
                zip.Save(response.OutputStream);
                response.Flush();
                response.End();
            }
        }
        #endregion
    }
    #endregion
}
#endregion