﻿#region NameSpaces
using System.Web.Mvc;
using System.Web.Routing;
#endregion
#region CommonCode
namespace CommonCode
{
    #region RouteConfig
    public class RouteConfig
    {
        #region RegisterRoutes
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Login", id = UrlParameter.Optional }
            );
        } 
        #endregion
    } 
    #endregion
} 
#endregion
