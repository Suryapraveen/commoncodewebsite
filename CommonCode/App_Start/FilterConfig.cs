﻿#region NameSpaces
using System.Web;
using System.Web.Mvc;
#endregion
#region CommonCode
namespace CommonCode
{
    #region FilterConfig
    public class FilterConfig
    {
        #region RegisterGlobalFilters
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        } 
        #endregion
    } 
    #endregion
}

#endregion