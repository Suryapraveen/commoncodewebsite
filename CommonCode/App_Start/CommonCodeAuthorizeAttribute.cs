﻿#region NameSpaces
using CommonCode.Common.Helpers;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
#endregion
#region CommonCode.App_Start
namespace CommonCode.App_Start
{
    #region CommonCodeAuthorizeAttribute
    public class CommonCodeAuthorizeAttribute : AuthorizeAttribute
    {
        #region CommonCodeAuthorizeAttribute
        public CommonCodeAuthorizeAttribute()
        {
        }
        #endregion

        #region AuthorizeCore
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isSessionExists = false;
            isSessionExists = UserInfo.UserId != 0;
            if (isSessionExists && UserInfo.UserId > 0)
                return true;
            else
                return isSessionExists;
        }
        #endregion

        #region HandleUnauthorizedRequest
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (UserInfo.UserId == 0)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Login" }, { "Action", "Login" } });
            }
        }
        #endregion

    }
    #endregion
}
#endregion