﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CommonCode.Startup))]
namespace CommonCode
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
