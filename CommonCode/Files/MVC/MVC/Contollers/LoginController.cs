﻿#region NameSpaces
using CommonCode.BusinessLayer.BAL;
using CommonCode.Common.Helpers;
using CommonCode.Models;
using System.Web.Mvc;
#endregion
#region CommonCode.Controllers
namespace CommonCode.Controllers
{
    #region LoginController
    [AllowAnonymous]
    public class LoginController : BaseController
    {
        #region Login
        public ActionResult Login()
        {
            return View();
        }
        #endregion

        #region ValidateUserLogin
        [HttpPost]
        public JsonResult ValidateUserLogin(LoginModel logEnty)
        {
            return Json(new UserBAL().ValidateUserLogin(logEnty), JsonRequestBehavior.AllowGet);
        } 
        #endregion

        #region Logout
        public ActionResult Logout()
        {
            Session.Abandon();
            UserInfo.UserId = 0;
            return RedirectToAction("Login", "Login");
        }
        #endregion
    } 
    #endregion
} 
#endregion