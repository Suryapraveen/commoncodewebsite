/****** Object:  StoredProcedure [dbo].[Common_ResetDatabase]    Script Date: 12/21/2016 22:53:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Common_ResetDatabase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Common_ResetDatabase]
GO

/****** Object:  StoredProcedure [dbo].[Common_ResetDatabase]    Script Date: 12/21/2016 22:53:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************************************************************
 
  EXEC Common_ResetDatabase
   
*******************************************************************************************************************************************************/

CREATE PROCEDURE [dbo].[Common_ResetDatabase]
AS
BEGIN
	SET NOCOUNT ON;
	
	EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL' ,@whereand=' and o.Name not like ''%master%'''
	EXEC sp_MSForEachTable 'DELETE  FROM ?', @whereand=' and o.Name not like ''%master%'''
	EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL' ,@whereand=' and o.Name not like ''%master%'''
	
	SELECT ROW_NUMBER() OVER  (ORDER BY TABLE_NAME) AS ROWID,
	    IDENT_SEED(TABLE_SCHEMA+'.'+TABLE_NAME) AS Seed,
	    IDENT_INCR(TABLE_SCHEMA+'.'+TABLE_NAME) AS Increment,
	    IDENT_CURRENT(TABLE_SCHEMA+'.'+TABLE_NAME) AS Current_Identity,
	    TABLE_SCHEMA+'.'+TABLE_NAME AS TABLENAME,
	    'DBCC CHECKIDENT('''+TABLE_SCHEMA+'.'+TABLE_NAME+''', RESEED,0)' AS STMT
	    INTO #TEMP
	FROM 
	    INFORMATION_SCHEMA.TABLES 
	WHERE OBJECTPROPERTY(OBJECT_ID(TABLE_SCHEMA+'.'+TABLE_NAME), 'TableHasIdentity') = 1 AND 
	      TABLE_TYPE = 'BASE TABLE' AND (TABLE_NAME NOT LIKE '%MASTER%' OR TABLE_NAME NOT LIKE '%sysdiagrams%')
	ORDER BY TABLE_SCHEMA, TABLE_NAME 
	
	DECLARE @ROWCNT INT,@I INT = 1,@STMT VARCHAR(600)
	SELECT @ROWCNT = MAX(ROWID) 
	FROM #TEMP
	
	WHILE @I<= @ROWCNT
	BEGIN
		 SELECT @STMT = STMT 
		 FROM #TEMP 
		 WHERE ROWID = @I
		 
		--PRINT @STMT
		 EXEC(@STMT)
		 
		 SET @I = @I+1
	END
	DROP TABLE #TEMP
	
END

GO


