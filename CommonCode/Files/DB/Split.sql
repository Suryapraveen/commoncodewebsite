/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 12/23/2016 09:44:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Split]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Split]
GO



/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 12/23/2016 09:44:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[Split](@STRING NVARCHAR(MAX), @DELIMITER CHAR(1)) RETURNS @TEMPTABLE TABLE (VALUE VARCHAR(8000))      
/***************************************************************************************************************************************************  
 FUNCTION  NAME      : SPLIT
 PROPROSE			 : TO  GET VALUES AS TABLE
 CREATED BY			 : SURYA PRAVEEN M  
 CREATED DATE        : APR 26 2012  
	
 USAGES :  	
	SELECT * FROM  DBO.Split('1,34',',')

*******************************************************************************************************************************************************/      
AS       
	BEGIN  
     DECLARE @IDX INT       
     DECLARE @SLICE VARCHAR(8000)       
     SELECT @IDX = 1       
        IF LEN(@STRING)<1 OR @STRING IS NULL  RETURN       
       
     WHILE @IDX!= 0       
     BEGIN       
         SET @IDX = CHARINDEX(@DELIMITER,@STRING)       
         IF @IDX!=0       
               SET @SLICE = LEFT(@STRING,@IDX - 1)       
          ELSE       
             SET @SLICE = @STRING       
           
          IF(LEN(@SLICE)>0) 
          BEGIN
				INSERT INTO @TEMPTABLE(VALUE) VALUES(@SLICE)   
		  END
         SET @STRING = RIGHT(@STRING,LEN(@STRING) - @IDX)       
         IF LEN(@STRING) = 0 BREAK       
     END   
  RETURN       
 END  
GO


