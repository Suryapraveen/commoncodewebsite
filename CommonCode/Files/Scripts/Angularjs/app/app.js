﻿var CommonCodeApp = angular.module('CommonCodeApp', ['ngMaterial', 'ngMessages', 'ngRoute'])
    .config(function ($routeProvider, $mdThemingProvider, $mdDateLocaleProvider) {
        $routeProvider
             .when('/', {
                  templateUrl: 'Login/Index',
                  controller: 'LoginController'
              })
        $mdThemingProvider.theme("success-toast");
        $mdThemingProvider.theme("error-toast");

    });


var StatusType = {
    SUCCESS: 0,
    ERROR: 1,
    WARNING: 2
}


var pCurrentPage = 0; //PageIndex
var pPageItems = 5;   //PageSize  
var pOrderBy = "";    //SortBy
var pOrderByReverse = false;  //SortDirection = 0

var PageSizeSelectedObj = { selected: 10 };

var DefaultDropDownValue = { selected: "Select" };
var errorMessage = '';
var getPageSizeList = function () {
    return [
               { value: 5, text: "5" },
               { value: 10, text: "10" },
               { value: 15, text: "15" },
               { value: 30, text: "30" },
               { value: 50, text: "50" },
               { value: 100, text: "100" }
    ];
}

getBaseUrl = function () {
    if (location.host.indexOf('localhost') >= 0) {
        return location.protocol + "//" + location.host;
    }
    else {
        return location.protocol + "//" + location.host + '/' + location.pathname.split('/')[1];
    }
}


