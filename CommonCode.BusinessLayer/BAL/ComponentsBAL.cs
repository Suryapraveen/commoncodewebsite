﻿#region NameSpaces
using CommonCode.Repositories.Contract;
using CommonCode.Repositories.Implementation;
using System.Collections.Generic;
#endregion
#region CommonCode.BusinessLayer.BAL
namespace CommonCode.BusinessLayer.BAL
{
    #region ComponentsBAL
    public class ComponentsBAL
    {
        #region Global Variables
        IComponentsRepository _compRepo;
        #endregion

        #region ComponentsBAL
        public ComponentsBAL()
        {
            _compRepo = new ComponentsRepository();
        }
        #endregion

        #region GetComponents
        public List<object> GetComponents()
        {
            return _compRepo.GetComponents();
        }
        #endregion
    }
    #endregion
}

#endregion