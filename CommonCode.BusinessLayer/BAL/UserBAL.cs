﻿#region NameSpaces
using CommonCode.Common.Helpers;
using CommonCode.Models;
using CommonCode.Repositories;
using CommonCode.Repositories.Contract;
using CommonCode.Repositories.Implementation;
using System;
#endregion
#region CommonCode.BusinessLayer.BAL
namespace CommonCode.BusinessLayer.BAL
{
    #region UserBAL
    public class UserBAL
    {
        #region Global Variables
        IUserRepository _userRepo;
        #endregion

        #region UserBAL
        public UserBAL()
        {
            _userRepo = new UserRepository();
        }
        #endregion

        #region ValidateUserLogin
        public LoginResultModel ValidateUserLogin(LoginModel logEnty)
        {
            ValidateUser_Result result = _userRepo.ValidateUserLogin(logEnty);
            if (result != null)
            {
                UserInfo.UserId = result.UserId;
                UserInfo.UserName = result.UserName;
                return new LoginResultModel()
                {
                    UserID = result.UserId,
                    URL = "/Projects/List",
                    Result = (Int32)result.Result
                };
            }
            return new LoginResultModel() { Result = -1, URL = "#" };
        }
        #endregion

    }
    #endregion
}

#endregion