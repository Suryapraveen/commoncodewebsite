﻿#region NameSpaces
using CommonCode.Common.Helpers;
using CommonCode.Entities;
using CommonCode.Repositories.Contract;
using CommonCode.Repositories.Implementation;
using System;
using System.Collections.Generic;
#endregion
#region CommonCode.BusinessLayer.BAL
namespace CommonCode.BusinessLayer.BAL
{
    #region ProjectsBAL
    public class ProjectsBAL
    {
        #region Global Variables
        IProjectsRepository _projectRepo;
        #endregion

        #region ProjectsBAL
        public ProjectsBAL()
        {
            _projectRepo = new ProjectsRepository();
        }
        #endregion

        #region GetProjectList
        public List<object> GetProjectList(GridSearchParameters projectSearch)
        {
            try
            {
                return _projectRepo.GetAllProects(projectSearch);
            }
            catch (Exception ex)
            {
                return new List<object>();
            }
        }
        #endregion

        #region SaveOrUpdateProejctInformation
        public bool SaveOrUpdateProejctInformation(ProjectModel pm)
        {
            try
            {
                return _projectRepo.SaveOrUpdateProejctInformation(pm);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
    #endregion
}

#endregion