﻿#region NameSpaces
using System;
#endregion
#region CommonCode.Common.Helpers
namespace CommonCode.Common.Helpers
{
    #region UserInfo
    public class UserInfo
    {
        #region UserInfo
        private UserInfo()
        {

        } 
        #endregion

        #region UserId
        public static Int64 UserId { get; set; }
        #endregion

        #region UserName
        public static string UserName { get; set; }
        #endregion

    }
    #endregion
} 
#endregion
