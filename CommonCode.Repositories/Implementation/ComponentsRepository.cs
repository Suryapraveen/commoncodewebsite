﻿#region NameSpaces
using CommonCode.Models;
using CommonCode.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion
#region CommonCode.Repositories.Implementation
namespace CommonCode.Repositories.Implementation
{
    #region ComponentsRepository
    public class ComponentsRepository : BaseRepository, IComponentsRepository
    {
        #region GetComponents
        public List<object> GetComponents()
        {
            List<object> lst = new List<object>();
            List<Components_GetAllComponents_Result> lstCompItems = dbContext.Components_GetAllComponents().ToList();
            var lstComponents = lstCompItems.Select(e => new { e.ComponentID, e.ComponentName }).Distinct().ToList();
            lst.Add(lstComponents);
            lst.Add(lstCompItems);
            return lst;
        }
        #endregion
    }
    #endregion
}

#endregion