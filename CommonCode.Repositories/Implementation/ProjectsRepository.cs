﻿#region NameSpaces
using CommonCode.Repositories.Contract;
using System.Collections.Generic;
using CommonCode.Common.Helpers;
using System;
using System.Linq;
using CommonCode.Entities;
using System.Data.Entity.Core.Objects;
#endregion
#region CommonCode.Repositories.Implementation
namespace CommonCode.Repositories.Implementation
{
    #region ProjectsRepository
    public class ProjectsRepository : BaseRepository, IProjectsRepository
    {
        #region GetAllProects
        public List<object> GetAllProects(GridSearchParameters projectSearch)
        {
            List<object> lst = new List<object>();
            GridHelper<Projects_GetList_Result> projectsList = new GridHelper<Projects_GetList_Result>();
            List<Projects_GetList_Result> lstProjects = dbContext.Projects_GetList(projectSearch.filterBy,
                projectSearch.orderBy ?? string.Empty, projectSearch.orderByReverse ?? string.Empty,
                Convert.ToInt32(projectSearch.pageItems), Convert.ToInt32(projectSearch.currentPage)).ToList();
            projectsList.ListOfItems = lstProjects;
            if (lstProjects.Count > 0)
                projectsList.TotalRecordCount = (int)lstProjects[0].TotalCount;
            lst.Add(projectsList);
            List<Components_GetAllComponents_Result> lstCompItems = dbContext.Components_GetAllComponents().ToList();
            var lstComponents = lstCompItems.Select(e => new { e.ComponentID, e.ComponentName,e.Selected }).Distinct().ToList();
            lst.Add(lstComponents);
            lst.Add(lstCompItems);
            return lst;
        }
        #endregion

        #region SaveOrUpdateProejctInformation
        public bool SaveOrUpdateProejctInformation(ProjectModel pm)
        {
            ObjectParameter output = new ObjectParameter("Result", typeof(int));
            dbContext.Projects_CRUDOperations(pm.ProjectId, pm.ProjectName, pm.ProjectDescription,
                UserInfo.UserName, UserInfo.UserId, pm.ProjectCompItems, output);
            return ((int)output.Value == 9999);
        }
        #endregion
    }
    #endregion
}

#endregion