﻿#region NameSpaces
#endregion
#region CommonCode.Repositories.Implementation
namespace CommonCode.Repositories.Implementation
{
    #region BaseRepository
    public class BaseRepository
    {
        #region Global Variables
        private CommonCodeWebSiteEntities _db;
        #endregion

        #region dbContext
        public CommonCodeWebSiteEntities dbContext
        {
            get
            {
                if (_db == null)
                {
                    _db = new CommonCodeWebSiteEntities();
                }
                return _db;
            }
        } 
        #endregion
    } 
    #endregion
}

#endregion