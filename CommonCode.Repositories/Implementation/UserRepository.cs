﻿#region NameSpaces
using System.Linq;
using CommonCode.Models;
using CommonCode.Repositories.Contract;
#endregion
#region CommonCode.Repositories.Implementation
namespace CommonCode.Repositories.Implementation
{
    #region UserRepository
    public class UserRepository : BaseRepository, IUserRepository
    {
        #region ValidateUserLogin
        public ValidateUser_Result ValidateUserLogin(LoginModel logEnty)
        {
            return dbContext.ValidateUser(logEnty.UserName, logEnty.Password).FirstOrDefault();
        } 
        #endregion

    } 
    #endregion
}

#endregion