﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CommonCode.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class CommonCodeWebSiteEntities : DbContext
    {
        public CommonCodeWebSiteEntities()
            : base("name=CommonCodeWebSiteEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ComponentItem> ComponentItems { get; set; }
        public virtual DbSet<ComponentItemType> ComponentItemTypes { get; set; }
        public virtual DbSet<Component> Components { get; set; }
        public virtual DbSet<ProjectComponentItem> ProjectComponentItems { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<User> Users { get; set; }
    
        public virtual int Common_ResetDatabase()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Common_ResetDatabase");
        }
    
        public virtual ObjectResult<Components_GetAllComponents_Result> Components_GetAllComponents()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Components_GetAllComponents_Result>("Components_GetAllComponents");
        }
    
        public virtual int Database_Backup(string name, string path)
        {
            var nameParameter = name != null ?
                new ObjectParameter("name", name) :
                new ObjectParameter("name", typeof(string));
    
            var pathParameter = path != null ?
                new ObjectParameter("path", path) :
                new ObjectParameter("path", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Database_Backup", nameParameter, pathParameter);
        }
    
        [DbFunction("CommonCodeWebSiteEntities", "fn_Split")]
        public virtual IQueryable<string> fn_Split(string sTRING, string dELIMITER)
        {
            var sTRINGParameter = sTRING != null ?
                new ObjectParameter("STRING", sTRING) :
                new ObjectParameter("STRING", typeof(string));
    
            var dELIMITERParameter = dELIMITER != null ?
                new ObjectParameter("DELIMITER", dELIMITER) :
                new ObjectParameter("DELIMITER", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<string>("[CommonCodeWebSiteEntities].[fn_Split](@STRING, @DELIMITER)", sTRINGParameter, dELIMITERParameter);
        }
    
        public virtual ObjectResult<GetAllUsers_Result> GetAllUsers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllUsers_Result>("GetAllUsers");
        }
    
        public virtual int InsertUpdateUsers(string modifiedBy)
        {
            var modifiedByParameter = modifiedBy != null ?
                new ObjectParameter("ModifiedBy", modifiedBy) :
                new ObjectParameter("ModifiedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertUpdateUsers", modifiedByParameter);
        }
    
        public virtual int Projects_CRUDOperations(Nullable<long> projectID, string projectName, string description, string projectOwner, Nullable<long> loggedUserId, string prjOptions, ObjectParameter result)
        {
            var projectIDParameter = projectID.HasValue ?
                new ObjectParameter("ProjectID", projectID) :
                new ObjectParameter("ProjectID", typeof(long));
    
            var projectNameParameter = projectName != null ?
                new ObjectParameter("ProjectName", projectName) :
                new ObjectParameter("ProjectName", typeof(string));
    
            var descriptionParameter = description != null ?
                new ObjectParameter("Description", description) :
                new ObjectParameter("Description", typeof(string));
    
            var projectOwnerParameter = projectOwner != null ?
                new ObjectParameter("ProjectOwner", projectOwner) :
                new ObjectParameter("ProjectOwner", typeof(string));
    
            var loggedUserIdParameter = loggedUserId.HasValue ?
                new ObjectParameter("LoggedUserId", loggedUserId) :
                new ObjectParameter("LoggedUserId", typeof(long));
    
            var prjOptionsParameter = prjOptions != null ?
                new ObjectParameter("PrjOptions", prjOptions) :
                new ObjectParameter("PrjOptions", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Projects_CRUDOperations", projectIDParameter, projectNameParameter, descriptionParameter, projectOwnerParameter, loggedUserIdParameter, prjOptionsParameter, result);
        }
    
        public virtual ObjectResult<Projects_GetList_Result> Projects_GetList(string globalFilterBy, string orderBy, string orderByReverse, Nullable<int> pageSize, Nullable<int> pageNumber)
        {
            var globalFilterByParameter = globalFilterBy != null ?
                new ObjectParameter("GlobalFilterBy", globalFilterBy) :
                new ObjectParameter("GlobalFilterBy", typeof(string));
    
            var orderByParameter = orderBy != null ?
                new ObjectParameter("OrderBy", orderBy) :
                new ObjectParameter("OrderBy", typeof(string));
    
            var orderByReverseParameter = orderByReverse != null ?
                new ObjectParameter("OrderByReverse", orderByReverse) :
                new ObjectParameter("OrderByReverse", typeof(string));
    
            var pageSizeParameter = pageSize.HasValue ?
                new ObjectParameter("PageSize", pageSize) :
                new ObjectParameter("PageSize", typeof(int));
    
            var pageNumberParameter = pageNumber.HasValue ?
                new ObjectParameter("PageNumber", pageNumber) :
                new ObjectParameter("PageNumber", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Projects_GetList_Result>("Projects_GetList", globalFilterByParameter, orderByParameter, orderByReverseParameter, pageSizeParameter, pageNumberParameter);
        }
    
        public virtual ObjectResult<ValidateUser_Result> ValidateUser(string loginOrEmail, string pASSWORD)
        {
            var loginOrEmailParameter = loginOrEmail != null ?
                new ObjectParameter("LoginOrEmail", loginOrEmail) :
                new ObjectParameter("LoginOrEmail", typeof(string));
    
            var pASSWORDParameter = pASSWORD != null ?
                new ObjectParameter("PASSWORD", pASSWORD) :
                new ObjectParameter("PASSWORD", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ValidateUser_Result>("ValidateUser", loginOrEmailParameter, pASSWORDParameter);
        }
    }
}
