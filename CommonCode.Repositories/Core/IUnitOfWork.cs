﻿using System;
namespace CommonCode.Repositories.Core
{
    /// <summary>
    /// Interface to perform all CRUD operations
    /// Override this method and use instance of the class 
    /// to perform a CRUD operation in Contorller class
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        //IVisitorRepository Visitors { get; }
        void Complete();
    }
}
