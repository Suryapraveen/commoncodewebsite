﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CommonCode.Repositories.Core.Repositories
{
    /// <summary>
    /// Centralised Repository Interface 
    /// to perform more Common Operations
    /// Such as Get, Find, Add, Delete
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : class 
    {
        /// <summary>
        /// Get an Entity by passing ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity Get(int id);
        
        /// <summary>
        /// Get All Entites
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Get Enumerable format for Enitities
        /// to be used in complex queries (joins)
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAllEnum();

        /// <summary>
        /// Get All Entities satisfying a particular condition
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        
        /// <summary>
        /// Get an Entity satisfying a particular condition
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);
        
        /// <summary>
        /// Add an Entity to DBSet
        /// </summary>
        /// <param name="entity"></param>
        void Add(TEntity entity);

        /// <summary>
        /// Add list of entities to DBSet
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Remove an Entity to DBSet
        /// </summary>
        /// <param name="entity"></param>
        void Remove(TEntity entity);

        /// <summary>
        /// Remove set of entities from DBSet
        /// </summary>
        /// <param name="entities"></param>
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}
