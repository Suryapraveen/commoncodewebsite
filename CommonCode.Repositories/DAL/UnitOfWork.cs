﻿using System;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity;
using CommonCode.Repositories.Core;
using CommonCode.Repositories;
using CommonCode.Repositories.Core.Repositories;
using CommonCode.Repositories.DAL.Repositories;

namespace CommonCode.Repositories.DAL
{
    /// <summary>
    /// Class to perform CRUD operations 
    /// by receiving a Database Connection object.
    /// Instantiate and use appropriate CRUD method in Controller class
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CommonCodeWebSiteEntities _dbContext;
        private readonly ObjectContext _objectContext;
        private readonly IDbTransaction _transaction;

        public UnitOfWork(CommonCodeWebSiteEntities context)
        {
            _dbContext = context;
            _objectContext = ((IObjectContextAdapter)_dbContext).ObjectContext;
            if (_objectContext.Connection.State != ConnectionState.Open)
            {
                try
                {
                    _objectContext.Connection.Open();
                    _transaction = _objectContext.Connection.BeginTransaction();
                }
                catch(Exception ex)
                {
                    _objectContext.Connection.Close();
                    //AccessWareLogger.Log(ex.ToString());
                }
            }
        }
        
        /// <summary>
        /// Repository to deal with Components
        /// </summary>
        public IComponentsRepository Components
        {
            get
            {
                return new ComponentsRepository(_dbContext);
            }
        }

        /// <summary>
        /// Repository to deal with Component Items
        /// </summary>
        public IComponentsRepository ComponentItems
        {
            get
            {
                return new ComponentsRepository(_dbContext);
            }
        }

        #region "Operations on repository"

        /// <summary>
        /// Save pending changes to database
        /// </summary>
        public void Complete()
        {
            try
            {
                _dbContext.SaveChanges();
                _transaction.Commit();
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        private void Rollback()
        {
            this._transaction.Rollback();

            foreach (var entry in _dbContext.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        /// <summary>
        /// Dispose current DBContext connection
        /// </summary>
        public void Dispose()
        {
            if(_objectContext != null)
            {
                if (_objectContext.Connection.State == ConnectionState.Open)
                {
                    _objectContext.Connection.Close();
                }
            }
        }
        #endregion
    }
}