﻿using System.Linq;
using CommonCode.Repositories;
using System.Data.Entity;
using CommonCode.Repositories.Core.Repositories;

namespace CommonCode.Repositories.DAL.Repositories
{
    public class ComponentsRepository : Repository<Component>, IComponentsRepository
    {
        public ComponentsRepository(CommonCodeWebSiteEntities context) 
            : base(context)
        {

        }

        ///// <summary>
        ///// return last or default visitor
        ///// </summary>
        //public Visitor LastOrDefault()
        //{
        //    return context.Visitors.AsEnumerable().LastOrDefault();
        //}

        //public CommonCodeWebSiteEntities context
        //{
        //    get { return Context as CommonCodeWebSiteEntities; }
        //}

    }
}