﻿using System.Linq;
using CommonCode.Repositories;
using System.Data.Entity;
using CommonCode.Repositories.Core.Repositories;

namespace CommonCode.Repositories.DAL.Repositories
{
    public class ComponentItemsRepository : Repository<ComponentItem>, IComponentItemsRepository
    {
        public ComponentItemsRepository(CommonCodeWebSiteEntities context) 
            : base(context)
        {

        }

        ///// <summary>
        ///// return last or default visitor
        ///// </summary>
        //public Visitor LastOrDefault()
        //{
        //    return context.Visitors.AsEnumerable().LastOrDefault();
        //}

        //public CommonCodeWebSiteEntities context
        //{
        //    get { return Context as CommonCodeWebSiteEntities; }
        //}

    }
}