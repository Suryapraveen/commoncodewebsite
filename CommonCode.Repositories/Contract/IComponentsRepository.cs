﻿#region NameSpaces
using CommonCode.Models;
using System.Collections.Generic;
#endregion
#region CommonCode.Repositories.Contract
namespace CommonCode.Repositories.Contract
{
    #region IComponentsRepository
    public interface IComponentsRepository
    {
        List<object> GetComponents();
    } 
    #endregion
}

#endregion