﻿#region NameSpaces
using CommonCode.Common.Helpers;
using CommonCode.Entities;
using System.Collections.Generic;
#endregion
#region CommonCode.Repositories.Contract
namespace CommonCode.Repositories.Contract
{
    #region IProjectsRepository
    public interface IProjectsRepository
    {
        List<object> GetAllProects(GridSearchParameters projectSearch);
        bool SaveOrUpdateProejctInformation(ProjectModel um);
    }
    #endregion
}

#endregion