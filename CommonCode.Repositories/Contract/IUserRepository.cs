﻿#region NameSpaces
using CommonCode.Models;
#endregion
#region CommonCode.Repositories.Contract
namespace CommonCode.Repositories.Contract
{
    #region IUserRepository
    public interface IUserRepository
    {
        ValidateUser_Result ValidateUserLogin(LoginModel logEnty);
    } 
    #endregion
}

#endregion