//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CommonCode.Repositories
{
    using System;
    using System.Collections.Generic;
    
    public partial class ComponentItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ComponentItem()
        {
            this.ProjectComponentItems = new HashSet<ProjectComponentItem>();
        }
    
        public long ComponentItemID { get; set; }
        public string ComponentItemName { get; set; }
        public Nullable<long> ComponentID { get; set; }
        public string Desription { get; set; }
        public string HelpText { get; set; }
        public string FolderName { get; set; }
        public string ResourceFilePath { get; set; }
        public Nullable<long> ParentComponentItemID { get; set; }
        public Nullable<long> ComponentItemTypeID { get; set; }
    
        public virtual ComponentItemType ComponentItemType { get; set; }
        public virtual Component Component { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectComponentItem> ProjectComponentItems { get; set; }
    }
}
