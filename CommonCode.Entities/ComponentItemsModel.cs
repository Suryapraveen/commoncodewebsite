﻿#region NameSpaces
#endregion
#region CommonCode.Models
namespace CommonCode.Models
{
    #region ComponentItemsModel
    public class ComponentItemsModel
    {
        public long ComponentID { get; set; }
        public long ComponentItemID { get; set; }
        public string ComponentItemName { get; set; }
        public string HelpText { get; set; }
        public bool Selected { get; set; }

    }
    #endregion
}

#endregion