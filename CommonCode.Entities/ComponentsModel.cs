﻿#region NameSpaces
using System.Collections.Generic;
#endregion
#region CommonCode.Models
namespace CommonCode.Models
{
    #region ComponentsModel
    public class ComponentsModel
    {
        public long ComponentID { get; set; }
        public string ComponentName { get; set; }
    } 
    #endregion
}

#endregion