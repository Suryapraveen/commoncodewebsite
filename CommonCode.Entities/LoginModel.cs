﻿#region NameSpaces
using System;
#endregion
#region CommonCode.Models
namespace CommonCode.Models
{
    #region LoginModel
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    #endregion

    #region LoginResultModel
    public class LoginResultModel
    {
        public Int32 Result { get; set; }
        public Int64 UserID { get; set; }
        public string URL { get; set; }
    } 
    #endregion
} 
#endregion