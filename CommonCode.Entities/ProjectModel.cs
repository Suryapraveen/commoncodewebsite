﻿#region NameSpaces
using System;
#endregion
#region CommonCode.Entities
namespace CommonCode.Entities
{
    #region ProjectModel
    public class ProjectModel
    {
        public Int64 ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public string ProjectCompItems { get; set; }
    }
    #endregion
}

#endregion